import React, { useState, useEffect } from 'react';

function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');

  useEffect(() => {
    const fetchAutomobiles = async () => {
      try {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
          const data = await response.json();

          setAutomobiles(data.automobiles || []);
        } else {
          setError('Failed to fetch automobiles');
        }
      } catch (error) {
        setError('Failed to fetch automobiles: ' + error.message);
      } finally {
        setLoading(false);
      }
    };

    fetchAutomobiles();
  }, []);

  return (
    <div>
      <h1>Automobiles</h1>
      {error && <p>{error}</p>}
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Year</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {loading ? (
            <tr>
              <td colSpan="6">Loading...</td>
            </tr>
          ) : automobiles.length === 0 ? (
            <tr>
              <td colSpan="6">No automobiles found</td>
            </tr>
          ) : (
            automobiles.map((auto) => (
              <tr key={auto.vin}>
                <td>{auto.vin}</td>
                <td>{auto.model.name}</td>
                <td>{auto.model.manufacturer.name}</td>
                <td>{auto.color}</td>
                <td>{auto.year}</td>
                <td>{auto.sold ? 'Yes' : 'No'}</td>
              </tr>
            ))
          )}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobileList;
