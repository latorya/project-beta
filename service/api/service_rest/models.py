from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.employee_id

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.pk})



class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=500)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=300)
    status = models.CharField(max_length=20)
    technician = models.ForeignKey(Technician, related_name="appointments", on_delete=models.PROTECT,)

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.pk})
